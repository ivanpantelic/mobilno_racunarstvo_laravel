<?php

namespace App\Http\Resources;

use App\Http\Resources\UsersCollection;
use Illuminate\Http\Resources\Json\Resource;

class AdsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category_id' => $this->category_id,
            'description' => $this->description,
            'user' => $this->user->name,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'img_url' => $this->img_url,
            'is_on_discount' => $this->is_on_discount,
            'discount' => $this->discount,
            'created_at' => strtotime($this->created_at),
            'updated_at' => strtotime($this->updated_at),
            'user_id' => $this->user_id,
        ];
    }
}

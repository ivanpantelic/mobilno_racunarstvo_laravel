<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response,Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Transaction;
use App\SmsNotification;
use App\Order;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

class StripeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('stripe');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function charges(Request $request)
    {
        try{
            $order = Order::whereId($request->get("order_id"))->with('order_items')->first();
            $order_item = $order->order_items->first();
            $smsNotification = $order_item->sms_notification;
            $amount = $request->get('amount') * 1;
            if($amount <= 3000) {
                $stripe = Stripe::charges()->create([
                    'currency' => 'USD',
                    'amount' => $request->get('amount') * 1,
                    'customer' => $request->get('customer'),
                    'description' => $request->get('note')
                ]);

                $message = $smsNotification->user_that_ordered()->first()->name . " je uplatio "
                     . $amount . ' USD na Vas racun. Vasa banka.';

                $basic  = new \Nexmo\Client\Credentials\Basic('0f821b0a', '2DXyCWCFrAqDD2zL');

                $client = new \Nexmo\Client($basic);

                $message = $client->message()->send([
                    'to' => '381638355745',
                    'from' => 'Nexmo',
                    'text' => $message
                ]);

            } 
            $transaction = Transaction::find($request->get('transaction_id'));
            $transaction->update(['status' => $request->get('status')]);
            
            return response()->json(["status" => true,"message" => "Payment successful."], 200)->
                    header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }catch(\Exception $e) {
            return response()->json(["status" => true,"message" => $e->getMessage()], 400)->
                    header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
    }


}

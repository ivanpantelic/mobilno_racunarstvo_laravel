<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Intervention\Image\ImageManagerStatic as Image;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use App\SmsNotification;
use App\Transaction;
use App\Order;
use App\User;
use DB;
use Auth;
use Filestack\FilestackClient;

class QRCodesController extends Controller
{

    public function store (Request $request) {
        
        $data = $request->all();
        if($transaction = new Transaction()) {
            DB::beginTransaction();
            try {
                // $transaction->save();
                $notifications = SmsNotification::all();
                foreach ($notifications as $notification) {
                    $user_to_notify = $notification->user_to_notify()->first();
                    $user_that_ordered = $notification->user_that_ordered()->first();
                    $order_item = $notification->order_item()->first();
                    $order = $order_item->order;
                   
                }
                $transaction->user_id_from = $user_that_ordered->id;
                $transaction->user_id_to = $user_to_notify->id;
                $transaction->order_id = $order->id;
                $transaction->price = $data["price"];
                $transaction->status = "Pending";
                $transaction->save();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    "status" => false,
                    "message" => $e->getMessage()
                ], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
            }

            return response()->json([
                "status" => true,
                "message" => "Transaction successfully created.",
                "transaction_id" => $transaction->id
            ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
    }

    public function update(Request $request) {

        $transaction = Transaction::find($request->get('transaction_id'));
        $transaction->update(['status' => $request->get('status')]);
        return response()->json([
            "status" => true,
            "message" => "Transaction successfully updated."
        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        // $data = 'K:PR|V:01|C:1|R:160000000000060216|N:Telekom Srbija A.D.
        // Takovska 2
        // Beograd|I:RSD2799,00|P:RADOVAN PANTELIĆ
        // VRELO BB
        // 14214 BANJANI|SF:189|S:MTS Račun 07/2020 2486885/2|RO:97152260142586358';
    }

    public function generate(Request $request) 
    {

        $order = Order::whereId($request->get("order_id"))->with('order_items')->first();
        $order_item = $order->order_items->first();
        $smsNotification = $order_item->sms_notification;
        
        $transaction = new Transaction();
        $transaction->user_id_from = $smsNotification->user_that_ordered()->first()->id;
        $transaction->user_id_to = $smsNotification->user_to_notify()->first()->id;
        $transaction->order_id = $order->id;
        $transaction->price = $request->total_amount;
        $transaction->status = "Pending";
        $transaction->save();

        $data = '{
            "primalac": "' .$smsNotification->user_to_notify()->first()->name .'",
            "platilac": "'.$smsNotification->user_that_ordered()->first()->name.'",
            "sifra_placanja": "289",
            "svrha_uplate": "Transakcije po nalogu gradjana",
            "u_korist_racuna": "'.$smsNotification->user_to_notify()->first()->account_number.'",
            "model_poziv": "",
            "iznos": "'.$request->total_amount .'",
            "valuta": "USD",
            "adresa": "'.$request->address.'",
            "napomena": "'.$request->note.'"
          }';
          
        $renderer = new \BaconQrCode\Renderer\Image\Png();
        $renderer->setHeight(256);
        $renderer->setWidth(256);
        $writer = new Writer($renderer);
        $img_url = '../public/uploads/images/'. $request->name . '.png';
        
        $writer->writeFile($data, $img_url);
    
        return response()->json(["status" => true,"message" => "Success", "url" => 'images/'. $request->name . '.png', "data" => $data,
                                'transaction_id'=>$transaction->id,'order_id' => $order->id]
                , 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }


    public function getDownload(Request $request)
    {

        // $file= public_path(). "\uploads\images\qr_test.png";
        // $img = Image::make($file);

        $image_name = $request['image_url'];
        $image_name = substr($image_name,7);
        $file = public_path() . '\uploads\images\\' . $image_name;
        $data = file_get_contents($file);
        $base64 = base64_encode($data);

        return response()->json(["status" => true,"message" => "Success", "base64" => $base64]
                , 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }

}

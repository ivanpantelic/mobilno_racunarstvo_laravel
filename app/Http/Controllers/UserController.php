<?php

namespace App\Http\Controllers;

use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterWelcomeMail;
use Auth;
use Hash;
use App\User;
use App\TokenTrait;
use App\Token;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Stripe;

class UserController extends Controller
{
    use TokenTrait;

    public function register(Request $request)
    {
        $validator = \Validator::make($request->all(),
        [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'mobile_phone' => 'required|numeric',
            'account_number' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422)->header('Access-Control-Allow-Origin','*');
        }

        try {
            DB::beginTransaction();
            $input = $request->all();
            
            $input['password'] = bcrypt($input['password']);
            $customer = Stripe::customers()->create([
                'email' => $input['email'],
                'description' => $input['name'],
                ]);
                
            User::create($input+['customer_id' => $customer['id']]);

            Mail::to($input['email'])->send(new RegisterWelcomeMail());
            DB::commit();
            
            return response()->json(["status" => true,"message" => "Registration successful.", "customer_id" =>$customer['id']], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        } catch(\Exception $e) {
            DB::rollback();
            return response()->json(["status" => true,"message" => $e->getMessage()], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
    }

    public function profileImage(Request $request)
    {
        if ($request->hasFile('photo')) {
            try {
                $photo = Storage::disk('public')->put('images/users', $request->file('photo'));
                return response()->json(["status" => true,"message" => "Upload successful", 'img_url' => $photo], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
            } catch(\Exception $e) {
                return response()->json(["status" => true,"message" => $e->getMessage()], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
            }
        } else {
            return response()->json(["status" => true,"message" => 'No photo uploaded'], 422)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
    }

    public function login(Request $request) {

        $validator = \Validator::make($request->all(),
        [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422)->header('Access-Control-Allow-Origin','*');
        }
        $user = User::where('email', $request['email'])->first();
        
        if(!Hash::check($request['password'], $user->password)) {
            $user = null;
        }
        
        
        if($user) {
            $passportToken = $this->createPassportTokenByUser($user, 2);
            $bearerToken = $this->sendBearerTokenResponse($passportToken['access_token'], $passportToken['refresh_token']);
            $bearerToken = json_decode($bearerToken->getBody()->__toString(), true);
            return response()->json([
                "status" => true,
                "message" => "Log in successful!",
                'customer_id' => $user->customer_id,
                $bearerToken,
            ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');

        } else {
            return response()->json([
                "status" => false,
                "message" => "Given credentials do not match our records.",
                "user" => null
            ], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
    }

    public function profile(Request $request)
    {
        $user_id = Auth::user()->id;

        $user = User::whereId($user_id)->first();

        return response()->json([
            "status" => true,
            "user" => $user
        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }

    public function logout(Request $request)
    {
        Token::where('user_id', Auth::user()->id)->delete();

        return response()->json([
            "status" => true,
            "message" => 'Log out successful!'

        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }

    public function changePassword(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        $user = Auth::user();
        if(Hash::check($request['currentPassword'], $user->password)) {
            if(strcmp($request['confirmPassword'],$request['newPassword']) == 0) {


            $user->password = bcrypt($request['newPassword']);
            $user->save();
            return response()->json([
                "status" => true,
                "message" => 'Password successfuly changed!'
            ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        } else {
            return response()->json([
                        "status" => false,
                        "message" => 'Confirm password does not match new password!'
                    ], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
        } else {

            return response()->json([
                "status" => false,
                "message" => 'Entered password does not match your current password!'
            ], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }

    }

    public function uploadPicture(Request $request)
    {
        try {
            $base64 = $request->get('code');
            $base64 = str_replace('data:image/jpeg;base64,', '', $base64);
            $data = base64_decode($base64);
            $fileName = Str::random(41);
            $photo = Storage::disk('public')->put('images/' . $request->get('context') . '/' . $fileName . '.jpg', $data);
            $img_url = 'images/' . $request->get('context'). '/' . $fileName . '.jpg';
            return response()->json(["status" => true,"message" => "Upload successful", 'img_url' => $img_url], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        } catch(\Exception $e) {
            return response()->json(["status" => true,"message" => $e->getMessage()], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Auth;
use App\Ad;
use App\Http\Resources\AdsCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::get();
        return response()->json([
            "status" => true,
            "ads" => AdsCollection::collection($ads)
        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),
        [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'category_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422)->header('Access-Control-Allow-Origin','*');
        }

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        Ad::create($data);


        return response()->json([
            "status" => true,
            "message" => 'Your ad has been successfully created.'
        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }

    public function adImage(Request $request)
    {
        if ($request->hasFile('photo')) {
            try {
                $photo = Storage::disk('public')->put('images/ads', $request->file('photo'));
                return response()->json(["status" => true,"message" => "Upload successful", 'img_url' => $photo], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
            } catch(\Exception $e) {
                return response()->json(["status" => true,"message" => $e->getMessage()], 400)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
            }
        } else {
            return response()->json(["status" => true,"message" => 'No photo uploaded'], 422)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::whereId($id)->get();
        return response()->json([
            "status" => true,
            "ad" => AdsCollection::collection($ad)
        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }

    public function getAllAds()
    {
        $user_id = Auth::user()->id;;
        $ads = Ad::where('user_id', $user_id)->get();

        return response()->json([
            "status" => true,
            "orders" => AdsCollection::collection($ads)
        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');
    }

    public function delete($id)
    {
        $user_id = Auth::user()->id;
        $ad = Ad::whereId($id)->delete();
        return response()->json([
            "status" => true,
            "message" => 'Ad successfuly deleted!'
        ], 200)->header('Content-Type', 'application/json')->header('Access-Control-Allow-Origin','*');

    }

    public function filter(Request $request)
    {
        $ad = null;
        if($category_id = $request->get('category_id')) $ad = Ad::where('category_id', $category_id);
        if($sort = $request->get('sort')) is_null($ad) ? $ad = Ad::orderBy($sort['key'], $sort['order']) : $ad->orderBy($sort['key'], $sort['order']);
        if($search = $request->get('search')) is_null($ad) ? $ad = Ad::where('title','LIKE','%'. $search .'%') : $ad->where('title','LIKE','%'. $search .'%');
        $ads = is_null($ad) ? Ad::all() : $ad->get();
        AdsCollection::wrap('ads');
        return AdsCollection::collection($ads);
    }

    public function applyDiscount(Request $request)
    {
        $this->validate($request, ['ad_id' => 'required', 'discount' => 'required|numeric|gt:0']);
        try {
            $ad = Ad::find($request->get('ad_id'));
            $this->validateApplyDiscount($ad, $request);
            $ad->update(['is_on_discount' => true,'discount' => $request->get('discount')]);
            return response()->json(['status' => true, 'message' => 'Discount successfully applied'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], $e->getCode());
        }
    }

    private function validateApplyDiscount(Ad $ad, Request $request)
    {
        if(is_null($ad)) throw new \Exception('Ad doesn\'t exist.', 404);
        if($ad->user_id != Auth::user()->id) throw new \Exception('This ad doesn\'t belong to you!', 400);
        if($ad->price < $request->get('discount')) throw new \Exception('Discount can\'t be greater than the ad\'s price.', 400);
        if($ad->is_on_discount) throw new \Exception('This ad is already on discount.', 400);;
    }

    public function removeDiscount(Request $request)
    {
        $this->validate($request, ['ad_id' => 'required']);
        try {
            $ad = Ad::find($request->get('ad_id'));
            $this->validateRemoveDiscount($ad, $request);
            $ad->update(['is_on_discount' => false,'discount' => null]);
            return response()->json(['status' => true, 'message' => 'Discount successfully removed'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], $e->getCode());
        }
    }

    private function validateRemoveDiscount(Ad $ad, Request $request)
    {
        if(is_null($ad)) throw new \Exception('Ad doesn\'t exist.', 404);
        if($ad->user_id != Auth::user()->id) throw new \Exception('This ad doesn\'t belong to you!', 400);
        if(!$ad->is_on_discount) throw new \Exception('This ad is not on discount.', 400);;
    }
}

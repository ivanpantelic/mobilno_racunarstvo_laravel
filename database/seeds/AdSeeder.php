<?php

use Illuminate\Database\Seeder;

class AdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->insert([
            'title' => 'The Heart Attack',
            'description'=> 'Only the best burger you will ever try !!',
            'price' => 20,
            'quantity' => 250,
            'user_id' => 1,
            'category_id' => 1,
            'img_url' => 'images/ads/burger.jpg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ads')->insert([
            'title' => 'AcDc\'s Greatest Album',
            'description'=> 'The Best of AcDc. Solid condition.',
            'price' => 2000,
            'quantity' => 2,
            'user_id' => 3,
            'category_id' => 2,
            'img_url' => 'images/ads/acdc.jpg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ads')->insert([
            'title' => 'Honda Civic',
            'description'=> 'ST 180 hp',
            'price' => 26500,
            'quantity' => 2,
            'user_id' => 1,
            'category_id' => 3,
            'img_url' => 'images/ads/honda_civic.jpg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ads')->insert([
            'title' => 'MacBook Pro',
            'description'=> '2019',
            'price' => 2599,
            'quantity' => 5,
            'user_id' => 2,
            'category_id' => 4,
            'img_url' => 'images/ads/macbook.jpg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ads')->insert([
            'title' => "Queen's First Album",
            'description'=> 'Original, unpacked album. It\'s a very rare chance to buy this item.',
            'price' => 7500,
            'quantity' => 1,
            'user_id' => 4,
            'category_id' => 2,
            'img_url' => 'images/ads/queen.jpg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ads')->insert([
            'title' => "Pizza",
            'description'=> 'The best pizza in the World!',
            'price' => 5,
            'quantity' => 100,
            'user_id' => 3,
            'category_id' => 1,
            'img_url' => 'images/ads/pizza.jpg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ads')->insert([
            'title' => "Peugeot 508",
            'description'=> 'The Ultimate driving machine!',
            'price' => 30500,
            'quantity' => 3,
            'user_id' => 2,
            'category_id' => 3,
            'img_url' => 'images/ads/peugeot.jpg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}

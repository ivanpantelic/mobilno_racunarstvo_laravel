<?php

use Illuminate\Http\Request;
use App\Mail\RegisterWelcomeMail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('/register', 'UserController@register');
    Route::post('/profile-image', 'UserController@profileImage');
    Route::post('/upload-picture', 'UserController@uploadPicture');
    Route::post('/ad-image', 'AdController@adImage');
    Route::post('/login', 'UserController@login');
    Route::get('stripe', 'StripeController@index');
    Route::post('/charges', 'StripeController@charges');
    Route::post('generate', 'QRCodesController@generate');
    Route::post('/transaction', 'QRCodesController@store');
    Route::post('/transaction-update', 'QRCodesController@update');
    Route::post('/getQR', 'QRCodesController@getDownload');
    Route::get('/email',function(){
        return new RegisterWelcomeMail();
    });

    Route::group(['middleware' => 'auth:api'], function () {

        //USERS
        Route::get('/profile', 'UserController@profile');
        Route::post('/logout', 'UserController@logout');
        Route::post('/change', 'UserController@changePassword');

        //CATEGORIES
        Route::get('/category', 'CategoryController@getCategories');

        //ADS
        Route::get('/ad', 'AdController@index');
        Route::post('/ad', 'AdController@store');
        Route::get('/ad/{ad}', 'AdController@show');
        Route::get('/ads','AdController@getAllAds');
        Route::post('/ads/filter','AdController@filter');
        Route::post('/ads/apply-discount','AdController@applyDiscount');
        Route::post('/ads/remove-discount','AdController@removeDiscount');

        //ORDERS
        Route::post('/order', 'OrderController@store');
        Route::get('/order', 'OrderController@index');

        Route::get('/temp_order_items', 'TempOrderItemController@index');
        Route::post('/temp_order_items', 'TempOrderItemController@store');
        Route::post('/temp_order_items/delete', 'TempOrderItemController@destroy');

        //PASSWORD RESETS
        Route::post('create', 'PasswordResetController@create');
        Route::get('find/{token}', 'PasswordResetController@find');
        Route::post('reset', 'PasswordResetController@reset');

        Route::post('/ads/{ad}', 'AdController@delete');

        
    });
});
